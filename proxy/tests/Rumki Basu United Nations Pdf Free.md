## Rumki Basu United Nations Pdf Free

 
  
 
**Download » [https://urevulad.blogspot.com/?d=2tySzy](https://urevulad.blogspot.com/?d=2tySzy)**

 
 
 
 
 
# A Review of Rumki Basu's Book on The United Nations
 
Rumki Basu is a professor of political science at Jamia Millia Islamia University in New Delhi, India. She has written several books on international relations, public administration, and governance. One of her most notable works is *The United Nations: Structure & Functions Of An International Organisation*, published by Sterling Publishers Pvt. Ltd in 2004.
 
This book provides a comprehensive overview of the history, structure, functions, and challenges of the United Nations (UN), the world's largest and most influential intergovernmental organization. Basu covers the origins and purposes of the UN, its membership and representation, its decision-making and administrative processes, its role in international law and human rights, its specialized agencies and programs, its peacekeeping and security operations, its regional conflicts and disarmament efforts, its development cooperation and social issues, and its achievements and limitations.
 
The book is divided into six parts: Part I introduces the concept and theories of international organization; Part II examines the evolution and structure of the UN; Part III analyzes the functions and performance of the UN; Part IV explores the issues and challenges facing the UN; Part V discusses the politics and economics of the emerging world order; and Part VI concludes with some proposals for reforming and strengthening the UN.
 
The book is written in a clear and concise style, with ample examples and references to support the arguments. It is suitable for students, scholars, practitioners, and general readers who are interested in learning more about the UN and its role in global affairs. The book also includes several appendices that provide useful information on the UN system, such as its charter, its human rights declaration, its court statute, and its member states.
 
One of the main strengths of the book is that it offers a balanced and critical perspective on the UN, highlighting both its achievements and its shortcomings. Basu acknowledges that the UN has made significant contributions to world peace, security, development, and cooperation, but also points out that it faces many problems and limitations, such as lack of representation, accountability, transparency, efficiency, effectiveness, legitimacy, and credibility. She argues that the UN needs to adapt to the changing realities of the 21st century and address the new challenges posed by globalization, terrorism, poverty, inequality, environmental degradation, human rights violations, and regional conflicts.
 
Another strength of the book is that it incorporates a global south perspective on the UN, reflecting on how the developing countries view and participate in the UN system. Basu emphasizes that the global south has a vital stake in the UN's success or failure, as it is often most affected by the issues that the UN deals with. She also examines how the global south has influenced and challenged the UN's agenda and policies, especially on matters related to development, human rights, democracy, security, and reform.
 
The book is not without some weaknesses. One weakness is that it does not provide enough analysis on some of the recent developments and events that have shaped or affected the UN's role and performance. For example, it does not discuss in detail how the UN responded to or handled some of the major crises or conflicts that occurred after 2004, such as the Iraq war, the Arab Spring, the Syrian civil war, the Libyan intervention, the Iranian nuclear deal, the North Korean missile tests, or the COVID-19 pandemic. Another weakness is that it does not offer enough comparison or contrast between the UN and other regional or sub-regional organizations that also play important roles in international politics.
 
Overall,*The United Nations: Structure & Functions Of An International Organisation*is a valuable and informative book that provides a comprehensive overview of the UN's history, structure, functions, and challenges. It offers a balanced and critical perspective on the UN's achievements and shortcomings, and incorporates a global south perspective on the UN's role and relevance. It is a useful resource for anyone who wants to learn more about the UN and its impact on global affairs.
 dde7e20689
 
 
